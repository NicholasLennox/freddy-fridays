import java.time.*;
import java.util.*;

class Friday {
    public static void main(String[] args) {
        showFriday13(2021);
        showMaxYears(2021, 3000);
    }

    public static void showFriday13(int year) {
        System.out.println("Showing all occurances of Friday the 13th in " + year);
        int occurances = 0;
        for (int month = 1; month <= 12; month++) {
            // Only checking every 13th of the month
            LocalDate d = LocalDate.of(year, month, 13);
            if (d.getDayOfWeek() == DayOfWeek.FRIDAY) {
                System.out.println("Friday the 13th in " + d.getMonth());
                occurances++;
            }
        }
        System.out.println("There were " + occurances + " Friday the 13ths in " + year);
    }

    public static void showMaxYears(int startYear, int endYear) {

        // Hashmap is used because its the fastest to search
        HashMap<Integer, Integer> fridays = new HashMap<Integer, Integer>();

        // Should refactor showFriday13 method to be able to be reused here
        for (int year = startYear; year <= endYear; year++) {
            int fridaysForYear = 0;
            for (int month = 1; month <= 12; month++) {
                LocalDate d = LocalDate.of(year, month, 13);
                if (d.getDayOfWeek() == DayOfWeek.FRIDAY) {
                    fridaysForYear++;
                }
            }
            fridays.put(year, fridaysForYear);
        }

        int maxFridays = Collections.max(fridays.values());

        System.out.println(
                "The maximum Friday the 13ths in the peroid " + startYear + " and " + endYear + " is " + maxFridays);
        System.out.println("The years which have this many Friday the 13ths are: ");

        List<Integer> yearsWithMaxFriday13 = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : fridays.entrySet()) {
            // Check if value matches with given value
            if (entry.getValue().equals(maxFridays)) {
                // Store the key from entry to the list
                yearsWithMaxFriday13.add(entry.getKey());
            }
        }
        System.out.println(yearsWithMaxFriday13);
    }
}